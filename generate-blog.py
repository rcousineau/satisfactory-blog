#!/usr/bin/env python

import os

class ImageSet:
    def __init__(self, name, title):
        self.name = name
        self.title = title
        self.thumbs = []
        self.thumb_names = {}
        self.images = []
        self.image_names = {}

        self.get_thumbs()
        self.get_images()
        self.check_thumbs()

    def get_thumbs(self):
        for thumb in sorted(os.listdir(f"src/images/{self.name}/thumbnails")):
            if not thumb.endswith(".jpg"):
                continue
            self.thumbs.append(thumb)
            self.thumb_names[thumb] = True

    def get_images(self):
        for img in sorted(os.listdir(f"src/images/{self.name}")):
            if not img.endswith(".jpg"):
                continue
            self.images.append(img)
            self.image_names[img] = True

    def check_thumbs(self):
        for thumb in self.thumbs:
            if not self.image_names.get(thumb):
                raise Exception(f"found thumbnail {thumb} with no accompanying image")
        for img in self.images:
            if not self.thumb_names.get(img):
                raise Exception(f"found image {img} with no accompanying thumbnail")

    def generate_links(self):
        links = []
        for img in self.images:
            thumb_path = f"images/{self.name}/thumbnails/{img}"
            image_path = f"images/{self.name}/{img}"
            links.append(f"[![{img}]({thumb_path})]({image_path})")
        print(f"generated {len(links)} links")
        md_path = f"src/{self.name}.md"
        print(f"writing to {md_path}")
        with open(md_path, "w") as blog:
            blog.write("<!-- this file generated with generate-blog.py -->\n")
            blog.write(f"## {self.title}\n\n")
            blog.write("\n".join(links))
        return f"[{self.title}](./{self.name}.md)"

def write_summary(items):
    summary_path = "src/SUMMARY.md"
    print(f"writing to {summary_path}")
    with open(summary_path, "w") as summary:
        summary.write("<!-- this file generated with generate-blog.py -->\n")
        summary.write("\n".join(items))

if __name__ == "__main__":
    generator = ImageSet("1.0", "1.0")
    opo_summary = generator.generate_links()
    generator = ImageSet("early-access", "Early Access")
    ea_summary = generator.generate_links()
    write_summary([opo_summary, ea_summary])
