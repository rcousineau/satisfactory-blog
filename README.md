# Satisfactory

[![pipeline status][pipeline_image]][pipeline]

[pipeline_image]: https://gitlab.com/rcousineau/satisfactory-blog/badges/master/pipeline.svg
[pipeline]: https://gitlab.com/rcousineau/satisfactory-blog/commits/master

The game [Satisfactory](https://www.satisfactorygame.com/) has risen to the
status where I now happily call it my favorite game. It's incredibly fun, the
scale of the factories you can build is astonishing, it looks fantastic, and
after more than 150 hours I feel like I've barely cracked the surface. So I
figured I'd just collect my favorite screenshots somewhere.

https://rcousineau.gitlab.io/satisfactory-blog/

---

This site is built using [mdBook][mdBook]. To build, first install mdBook with cargo:

```sh
$ cargo install mdbook
```

Then build it (the default output dir is `book`):

```sh
$ mdbook build
```

Or run a lightweight http server and watch for source changes:

```sh
$ mdbook serve
```

[mdBook]: https://github.com/rust-lang-nursery/mdBook

---

This mdBook is generated using the following commands:

```sh
cp /c/Program\ Files\ \(x86\)/Steam/userdata/17554713/760/remote/526870/screenshots/*.jpg src/images/
cp /c/Program\ Files\ \(x86\)/Steam/userdata/17554713/760/remote/526870/screenshots/thumbnails/*.jpg src/images/thumbnails
python generate-blog.py
```

This script:

* validates that for every screenshot there is a thumbnail (and vice versa)
* generates an image link in markdown syntax using the screenshot and thumbnail
* writes the link to `src/book.md`
